# Introduccción a los Sistemas Operativos

## 2020 Semestre 2 - Jueves

En este respositorio se irá publicando material de la materia (slides, código, tutoriales, etc).


## Listas

- tpi-est-so@listas.unq.edu.ar
        Dudas, consultas, avisos para comunicación con todos los estudiantes y docentes.

- tpi-doc-so@listas.unq.edu.ar
        Cualquier duda de la materia que no es para todos los compañeros, envíenla a esta lista


## Libros usados en la cursada

- Modern Operating Systems (Tanenbaum)
- Operating System Concepts (Silberschatz)


## Slides de las Clases
- [0 - Curso](./slides/00_curso.pdf)



## Trabajos Prácticos

Durante la materia iremos trabajando sobre un simulador básico de un sistema operativo que nos permita entender los conceptos que iremos desarrollando a lo largo de las clases. Como hemos mencionado, a esta altura carecemos de las herramientas para trabajar sobre un sistema operativo, aún de propósito didáctico como por ejemplo NachOS. Por tal motivo elegimos trabajar con un simulador que nos permita enfocarnos en los conceptos estudiados.

- [Intro a Python](./python/python_intro.md)

Breve (muy breve) introducción a Python y al trabajo práctico.

- [Ejemplos](./python/examples)

Algunos ejemplos de Python utilizados en la Introducción.



### Git

- [Git tutorial](http://rogerdudler.github.io/git-guide/)
- [Git](./slides/00_git.pdf)




## Slides de Prácticas






